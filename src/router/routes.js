const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "/", component: () => import("pages/Home.vue") },
      { path: "/schedule", component: () => import("pages/Schedule.vue") },
      { path: "/management", component: () => import("pages/Management.vue") },
      { path: "/professors", component: () => import("pages/Professors.vue") },
      { path: "/profile", component: () => import("pages/Profile.vue") },
      { path: "/logout", component: () => import("pages/Logout.vue") }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
